/**
 * @format
 */

import {AppRegistry} from 'react-native';
import EX_1_1_0 from './src/study/UI_1.1.0';
import EX_1_1_1 from './src/study/UI_1.1.1';
import EX_1_2_0 from './src/study/UI_1.2.0';
import EX_1_2_0_1 from './src/study/UI_1.2.0_1';
import EX_1_2_1 from './src/study/UI_1.2.1';
import EX_Text_Input from './src/study/InputText';
import Test from './src/study/test';
import compareComponent from './src/study/compare_Pure_Component';
import {name as appName} from './app.json';


// AppRegistry.registerComponent(appName, () => EX_1_1_0);
// AppRegistry.registerComponent(appName, () => EX_1_1_1);
// AppRegistry.registerComponent(appName, () => EX_1_2_0);
// AppRegistry.registerComponent(appName, () => EX_1_2_0_1);
// AppRegistry.registerComponent(appName, () => EX_Text_Input);
AppRegistry.registerComponent(appName, () => EX_1_2_1);

// AppRegistry.registerComponent(appName, () => Test);
// AppRegistry.registerComponent(appName, () => compareComponent);
