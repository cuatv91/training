import {Button} from 'react-native';
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Alert, TextInput} from 'react-native';

type Props = {};
export default class App extends Component {
  constructor(props) {
    super(props);
    // console.log('eeeee');
    this.state = {
      text: 'Hello ',
      text2: '',
      // aaa: 'nânna'
    };
    //   this.handleChange = this.handleChange.bind(this);
  }
  onPress = () => {
    // console.log("current ==> ", this.state);

    //  console.log("ref",this.refs.yourName.value)
    this.setState({
      text2: this.state.text,
    });

    // this.state.is;
    //console.log(this.state);
  };

  handleChange(text) {
    // console.log('update => ', text);
    this.setState({
      text: text,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>your input is: {this.state.text2}</Text>
        <Button onPress={this.onPress} title="Click Me " />
        <TextInput
          placeholder="Type your name here"
          onChangeText={text => this.handleChange(text)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    borderColor: 'black',
    borderWidth: 1,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
