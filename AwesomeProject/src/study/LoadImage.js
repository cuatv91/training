import React, {Component} from 'react';
import {Image} from 'react-native';

export default class LoadImage extends Component {
  render() {
    const imageSource = {
      uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png'
    };
    return <Image source={imageSource} style={{width: 300, height: 300}} />;
    )
  }
}