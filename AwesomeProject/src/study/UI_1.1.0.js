/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {useState, Component} from 'react';
import {StyleSheet, View, Text, Button, TextInput, Image} from 'react-native';
// import {tsConstructorType} from '@babel/types';

export default function App() {
  const [enteredGoal, setEnteredGoal] = useState('');
  const [courseGoals, setCourseGoals] = useState('KKK');
  const imageSource = {uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png',};
  const handleInput = enteredText => {
    setEnteredGoal(enteredText);
  };

  // const addInput = () => {
  //   console.log(enteredInput);
  // };

  const addGoals = () => {
    setCourseGoals(goals => [...goals, enteredGoal]);
  };

  return (
    <View style={styles.screen}>
      <View>
        <Text style={styles.header}>Hello</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="placeholder"
          style={styles.input}
          onChangeText={handleInput}
          value={enteredGoal}
        />
        <Button title="ADD" onPress={addGoals} />
      </View>
      {/* <View>
        {courseGoals.map(goal => (
          <Text key={goal}>{goal}</Text>
        ))}
      </View> */}
      <View>
        <Text style={styles.textDis}>your input is {enteredGoal}</Text>
      </View>
      <Image source={imageSource} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screen: {
    padding: 50,
  },
  inputContainer: {
    paddingBottom: 50,
    paddingTop: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    borderColor: 'black',
    borderWidth: 1,
    padding: 10,
  },
  textDis: {
    fontSize: 30,
    color: 'red',
  },
  header: {
    fontSize: 30,
    color: 'green',
    textAlign: 'center',
  },
});
