import React, {Component} from 'react';
import {Text} from 'react-native';

export default class TextHelloWorld extends Component {
  render() {
    const greeting = 'Hello World';
    return <Text>{greeting}</Text>;
  }
}
