import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  Alert,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';

export default class ViewContainer extends Component {
  _onShowAlert() {
    Alert.alert('Hello world');
    console.log('click!');
  }
  constructor(props) {
    super(props);
    this.state = {text: 'xxx'};
  }
  render() {
    const greeting = 'Hello World';
    const greetingDefault = 'Hello World';
    const imageSource = {
      uri: 'https://facebook.github.io/react-native/docs/assets/favicon.png',
    };
    return (
      <View>
        <ScrollView style={styles.scrollView}>
          <View style={styles.view_Container}>
            <Text>{greeting}</Text>
            <TextInput
              placeholder={greetingDefault}
              style={styles.inputText}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
            <Image source={imageSource} style={styles.image_Favicon} />
          </View>
          <View>
            <Button
              title="ADD"
              onPress={this._onShowAlert}
              style={styles.fixToText}
            />
          </View>
          <View style={styles.view_Container}>
            {/* Click -  mờ button trong xx s*/}
            <TouchableOpacity
              style={styles.button_Opacity}
              activeOpacity={0.5}
              onPress={() => this._onShowAlert()}>
              <Text style={styles.text_Button}>TouchableOpacity</Text>
            </TouchableOpacity>
            {/* Click - hight light button*/}
            <TouchableHighlight
              style={styles.button_Hightlight}
              underlayColor={'black'}
              onPress={() => this._onShowAlert()}>
              <Text style={styles.text_Button}>TouchableHighlight</Text>
            </TouchableHighlight>
            {/* Click -  do nothing*/}
            <TouchableWithoutFeedback
              // delayLongPress={5}
              onLongPress={() => this._onShowAlert()}>
              <View style={styles.button_WithoutFeedback}>
                <Text style={styles.text_Button}>TouchableWithoutFeedback</Text>
              </View>
            </TouchableWithoutFeedback>

            {/* Click -  props*/}
            <View>
              <GreetingUser name={this.state.text} />
            </View>

            {/* Click -  state1*/}
            {/* <View>
              <Blink text={this.state.text} />
            </View> */}

            {/* Click -  state2*/}
            <View style={styles.view_Container}>
              <AgeUser name={'Hoa'} age={18} />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

class GreetingUser extends Component {
  render() {
    const greetingName = `Hello ${this.props.name}`;
    return (
      <View style={styles.view_Container1}>
        <Text>{greetingName}</Text>
      </View>
    );
  }
}

class AgeUser extends Component {
  constructor(props123) {
    super(props123);
    this.state = {
      name: props123.name,
      number: props123.age,
    };
    console.log(this.state);
  }

  _onNumberPlus1() {
    this.setState({
      number: this.state.number + 1,
    });
    this.state.is;
    console.log(this.state);
    let x = 10;
    console.log(x);
  }

  render() {
    const textHead = `Name:${this.state.name}, Age: ${this.state.number}`;
    return (
      <View style={styles.view_Container1}>
        <Text>{textHead}</Text>

        <TouchableHighlight
          style={styles.button_Opacity}
          onPress={() => this._onNumberPlus1()}>
          <Text style={styles.text_Button}>Plus 1</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
class Blink extends Component {
  componentDidMount() {
    // Toggle the state every second
    setInterval(
      () =>
        this.setState(previousState => ({
          isShowingText: !previousState.isShowingText,
        })),
      1000,
    );
  }

  //state object
  state = {isShowingText: true};

  render() {
    if (!this.state.isShowingText) {
      return null;
    }

    return <Text>CHÀO {this.props.text}</Text>;
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: 'pink',
    marginHorizontal: 20,
  },
  image_Favicon: {
    width: 100,
    height: 100,
  },
  inputText: {
    borderColor: 'black',
    borderWidth: 1,
    width: 200,
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  view_Container1: {
    backgroundColor: '#F5FCFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 20,
  },
  button_Hightlight: {
    backgroundColor: 'green',
  },
  button_Opacity: {
    backgroundColor: 'red',
  },
  button_WithoutFeedback: {
    //flex: 1,
    backgroundColor: 'yellow',
  },
  text_Button: {
    color: 'black',
    padding: 20,
    fontSize: 18,
  },
  view_Container2: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    // flexDirection: 'column', // default
    flexDirection: 'row',
    // flexDirection: 'row-reverse',
    // flexDirection: 'column-reverse',

    // justifyContent: 'flex-start', // default
    // justifyContent: 'flex-end',
    justifyContent: 'center',
    // justifyContent: 'space-between',
    // justifyContent: 'space-around',

    // alignItems: 'stretch', // default
    // alignItems: 'flex-start',
    // alignItems: 'flex-end',
    alignItems: 'center',
    // alignItems: 'baseline',

    // flexWrap: 'wrap', // Can't wrap with %
    // flexWrap: 'nowrap',

    // alignContent: 'flex-end',
    // alignContent: 'stretch',
    // alignContent: 'center',
    // alignContent: 'space-between',
    // alignContent: 'space-around'
  },
  view_Children: {
    position: 'absolute',
    height: '30%',
    width: '35%',
  },
  background_red: {
    backgroundColor: 'red',
    height: '80%',
    zIndex: 1,
    top: 20,
    left: 80,
    bottom: 20,
  },
  background_blue: {
    backgroundColor: 'blue',
    height: '40%',
    zIndex: 2,
    top: 20,
    left: 40,
    bottom: 20,
  },
  background_black: {
    backgroundColor: 'black',
    bottom: 40,
    left: 150,
  },
  view_Children_Add: {
    // alignSelf: 'center',
    // alignSelf: 'stretch',
    // alignSelf: 'flex-start',
    // alignSelf: 'flex-end',
    // alignSelf: 'baseline'
  },
});

// export default class ViewContainer extends Component {
//   render() {
//     return (
//       <View style={styles.view_Container2}>
//         <View style={[styles.view_Children, styles.background_red, styles.view_Children_Add]} />
//         <View style={[styles.view_Children, styles.background_blue]} />
//         <View style={[styles.view_Children, styles.background_black, styles.view_Children_Add]} />
//         {/* <View style={[styles.view_Children, {backgroundColor: 'bisque'}]}/>
//         <View style={[styles.view_Children, {backgroundColor: 'cadetblue'}]}/>
//         <View style={[styles.view_Children, {backgroundColor: 'chocolate'}]}/>
//         <View style={[styles.view_Children, {backgroundColor: 'green'}]}/>
//         <View style={[styles.view_Children, {backgroundColor: 'pink'}]}/>
//         <View style={[styles.view_Children, {backgroundColor: 'yellow'}]}/> */}
//       </View>
//     );
//   }
// }
