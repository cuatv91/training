import React, {Component, PureComponent} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  Button,
} from 'react-native';

export default class ViewContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {clicked: false};
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    this.setState({clicked: !this.state.clicked});
  }

  render() {
    console.log('Start');
    return (
      <View>
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => this.onClick()}
          style={{backgroundColor: 'red'}}>
          <Text>button {this.state.clicked && 'clicked'}</Text>
        </TouchableOpacity>

        <TestPureComponent />
      </View>
    );
  }
}

class TestPureComponent extends PureComponent {
// class TestPureComponent extends Component {
  render() {
    console.log('Child');
    return <View><Text>xxx</Text></View>;
  }
}
