import React, {Component as PureComponent} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import styles from '../styles/style_1_2_1';

export default class ViewContainer extends PureComponent {
  constructor(props) {
    super(props);
    // this.exampleRef = React.createRef();
    this.state = {
      text: 'please input text ',
      text2: '2',
      text3: '3',
      text4: '4',
    };
  }
  onPress_highlight = () => {
    this.setState({
      text2: this.state.text,
    });
  };
  onPress_Opacity = () => {
    this.setState({
      text3: this.state.text,
    });
  };
  onPress_WF = () => {
    this.setState({
      text4: this.state.text,
    });
  };

  handleChange(text) {
    // console.log('update => ', text);
    this.setState({
      text: text,
    });
  }

  render() {
    const textDisplay =
      'A wrapper for making views respond properly to touches. On press down, the opacity of the wrapped view is decreased, which allows the underlay color to show through, darkening or tinting the view.';
    return (
      <View style={styles.view_Container}>
        <ScrollView style={styles.scrollView}>
          {/* Area 1:
            display text in 1 line
            text input - 1 line => click button type TouchableHighlight  => display text input at text area */}
          <View style={styles.areaView}>
            <Text numberOfLines={1}>TouchableHighlight area:{textDisplay}</Text>
            <Text style={styles.show_text} numberOfLines={1}>
              {this.state.text2}
            </Text>
            <View style={styles.childView}>
              <View>
                <TextInput
                  style={styles.input_Text}
                  onChangeText={text => this.handleChange(text)}
                />
              </View>
              <View style={styles.button_Opacity}>
                <TouchableHighlight
                  // style={styles.button_Opacity}
                  activeOpacity={0.5}
                  underlayColor={'yellow'}
                  onPress={this.onPress_highlight}>
                  <Text>Show</Text>
                </TouchableHighlight>
              </View>
            </View>
          </View>

          {/* Area 2:
            display text in 2 line
            text input many line => click button type TouchableHighlight  => display text input at text area on 1 line*/}
          <View style={styles.areaView}>
            <Text numberOfLines={2}>TouchableOpacity area: {textDisplay}</Text>
            <Text style={styles.show_text} numberOfLines={2}>
              {this.state.text3}
            </Text>
            <View style={styles.childView}>
              <View>
                <TextInput
                  editable
                  maxLength={100}
                  multiline
                  numberOfLines={4}
                  style={styles.input_Text_Multiline}
                  onChangeText={text => this.handleChange(text)}
                  // ref={this.exampleRef}
                  // ref={component => this._textInput = component}
                />
              </View>
              <View style={styles.button_Opacity}>
                <TouchableOpacity
                  // style={styles.button_Opacity}
                  onPress={this.onPress_Opacity}>
                  <Text>Show</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          {/* Area 3:
            display text in 2 line
            text input many line => click button type TouchableHighlight  => display text input at text area on 1 line*/}
          <View style={styles.areaView}>
            <Text numberOfLines={2}>
              TouchableWithoutFeedback area: {textDisplay}
            </Text>
            <Text style={styles.show_text} numberOfLines={2}>
              {this.state.text4}
            </Text>
            <View style={styles.childView}>
              <View>
                <TextInput
                  editable
                  maxLength={100}
                  multiline
                  numberOfLines={4}
                  style={styles.input_Text_Multiline}
                  onChangeText={text => this.handleChange(text)}
                />
              </View>
              <View style={styles.button_Opacity}>
                <TouchableWithoutFeedback
                  delayLongPress={5}
                  onLongPress={this.onPress_Opacity}>
                  <Text>Show</Text>
                </TouchableWithoutFeedback>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
