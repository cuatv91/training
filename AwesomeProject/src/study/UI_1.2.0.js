import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  Alert,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';

export default class ViewContainer extends Component {
  render() {
    return (
      <View style={styles.view_Container2}>
        <View style={[styles.view_Children, styles.background_white]} />
        <View style={[styles.view_Children, styles.background_r_pink]}>
          <Text style={styles.text_content}>#1DA3AE</Text>
        </View>
        <View style={[styles.view_Children, styles.background_l_pink]}>
          <Text style={styles.text_content}>#F59062</Text>
        </View>
        <View style={[styles.view_Children, styles.background_blue]}>
          <Text style={styles.text_content}>#F59062</Text>
        </View>
        <View style={[styles.view_Children, styles.background_black]}>
          <Text style={styles.text_content}>#948A8A</Text>
        </View>
        <View style={[styles.view_Children, styles.background_red]} />
        <View style={[styles.view_Children, styles.background_white2]}>
          <Text style={styles.text_content}>#CF5353</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view_Container2: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  view_Children: {
    position: 'absolute',
  },
  background_white: {
    borderColor: 'black',
    borderWidth: 1,
    height: '22%',
    top: 80,
    left: 20,
    right: 20,
    bottom: 40,
  },
  background_white2: {
    borderColor: 'black',
    borderWidth: 1,
    height: 240,
    left: 30,
    bottom: 45,
    width: 200,
    borderRadius: 10,
  },
  background_red: {
    backgroundColor: '#CF5353',
    height: 100,
    left: 250,
    width: 100,
    bottom: 115,
    borderRadius: 10,
  },
  background_r_pink: {
    backgroundColor: '#F59062',
    height: '10%',
    top: 145,
    left: '52%',
    bottom: 20,
    right: 35,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  background_l_pink: {
    backgroundColor: '#F59062',
    height: '10%',
    top: 145,
    right: '52%',
    bottom: 20,
    left: 35,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  background_blue: {
    backgroundColor: '#1DA3AE',
    height: 35,
    top: 95,
    width: 200,
    bottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  background_black: {
    backgroundColor: '#948A8A',
    height: 90,
    top: 250,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text_content: {
    fontSize: 20,
    color: 'white',
  },
});
