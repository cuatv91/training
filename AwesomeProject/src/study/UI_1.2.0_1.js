import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';

export default class ViewContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {text: 'xxx'};
  }
  render() {
    return (
      <View style={styles.view_Container}>
        {/* <ScrollView style={styles.scrollView}> */}
        <View style={styles.area1}>
          <TextInput
            style={styles.inputText}
            onChangeText={text => this.setState({text})}
            value={this.state.text}
          />
        </View>
        <View style={styles.area2}>
          <View style={[styles.view_Children, styles.borderRadius]}>
            <Image source={require('../Image/image_1.png')} />
            <Text style={styles.textBold}>Diet Recommendation</Text>
          </View>
          <View style={[styles.view_Children, styles.borderRadius]}>
            <Image source={require('../Image/image_2.png')} />
            <Text style={styles.textBold}>Kegel Exercises</Text>
          </View>
          <View style={[styles.view_Children, styles.borderRadius]}>
            <Image source={require('../Image/image_3.png')} />
            <Text style={styles.textBold}>Meditation</Text>
          </View>
          <View style={[styles.view_Children, styles.borderRadius]}>
            <Image source={require('../Image/image_4.png')} />
            <Text style={styles.textBold}>Yoga</Text>
          </View>
        </View>
        <View style={styles.area3}>
          <Image
            source={require('../Image/image_8.png')}
            style={styles.image_lock}
          />
          <View style={styles.textView}>
            <Text style={styles.text1}>Basics 2</Text>
            <Text style={styles.text2}>Start your depen you practice</Text>
          </View>
        </View>
        <View style={styles.area4}>
          <View style={styles.view_Children1}>
            <Image
              source={require('../Image/image_5.png')}
              style={styles.image_lock1}
            />
            <Text style={styles.textBold}>Today</Text>
          </View>
          <View style={styles.view_Children1}>
            <Image
              source={require('../Image/image_6.png')}
              style={styles.image_lock1}
            />
            <Text style={styles.text3}>All Excercises</Text>
          </View>
          <View style={styles.view_Children1}>
            <Image
              source={require('../Image/Setting_off.png')}
              style={styles.image_lock1}
            />
            <Text style={styles.textBold}>Settings</Text>
          </View>
        </View>
        {/* </ScrollView> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: '#dbe1e3',
  },
  view_Container: {
    flex: 1,
    backgroundColor: '#dbe1e3',
  },
  area1: {
    alignItems: 'center',
    backgroundColor: '#F5CFB4',
    height: 90,
  },
  area3: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    left: 21,
    paddingLeft: 25,
    width: '90%',
    borderRadius: 10,
  },
  area4: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  area2: {
    paddingTop: 20,
    flex: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    alignContent: 'space-around',
    paddingLeft: 25,
    paddingRight: 25,
    paddingBottom: 20,
  },
  view_Children: {
    height: '45%',
    width: '45%',
    backgroundColor: '#FFFFFF',
    paddingBottom: 12,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
  },
  view_Children1: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: 20,
  },
  borderRadius: {
    borderRadius: 10,
  },
  inputText: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 300,
    height: 40,
    borderRadius: 15,
    top: 35,
  },
  textBold: {
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  textView: {
    flex: 10,
  },
  text1: {
    fontSize: 20,
    fontWeight: 'bold',
    left: 20,
  },
  text3: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#e86e5f',
    textAlign: 'center',
  },
  text2: {
    fontSize: 20,
    left: 20,
  },
  image_lock: {
    flex: 1,
  },
  image_lock1: {},
});
