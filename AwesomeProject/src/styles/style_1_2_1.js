import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  view_Container: {
    flex: 1,
    fontFamily: 'Cochin',
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'pink',
    marginHorizontal: 30,
    marginVertical: 30,
  },
  areaView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    margin: 20,
    borderBottomWidth: 2,
  },
  childView: {
    flexDirection: 'row',
    paddingVertical: 10,
    // justifyContent: 'center',
    alignItems: 'center',
  },
  show_text: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: '90%',
    height: 40,
    top: 35,
  },
  input_Text: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 200,
    height: 40,
    borderRadius: 10,
  },
  input_Text_Multiline: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 200,
    height: 70,
    borderRadius: 10,
    textAlignVertical: 'top',
  },
  padding_bt: {
    paddingBottom: 20,
  },
  button_Opacity: {
    backgroundColor: 'red',
    borderRadius: 10,
    width: 70,
    height: 40,
    alignItems: 'center',
    paddingTop: 10,
    marginLeft: 10,
  },
});
