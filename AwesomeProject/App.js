import React, { Component } from 'react'
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  Alert,
  Button,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';

export default class ViewContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {text: 'xxx'};
  }
  _onShowAlert() {
    Alert.alert('Hello world');
    console.log('click!');
  }
  render() {
    return (
      <View style={styles.view_Container}>
        <ScrollView style={styles.scrollView}>
          <View style={styles.areaView}>
            <Text style={styles.show_text}> </Text>
            <TextInput
              style={styles.input_Text}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
            <TouchableOpacity
              style={styles.button_Opacity}
              activeOpacity={0.5}
              onPress={() => this._onShowAlert()}>
              <Text style={styles.text_Button}>TouchableOpacity</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.areaView}>
            <Text style={styles.show_text}>ssss1</Text>
            <TextInput
              style={styles.input_Text}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
            <TextInput
              style={styles.input_Text}
              onChangeText={text => this.setState({text})}
              value={this.state.text}
            />
          </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  view_Container: {
    flex: 1,
    fontFamily: 'Cochin',
  },
  scrollView: {
    flex: 1,
    backgroundColor: 'pink',
    marginHorizontal: 30,
    marginVertical: 60,
  },
  areaView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    flexWrap: 'wrap',
    margin: 20,
  },
  show_text: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: '80%',
    height: 40,
    top: 35,
  },
  input_Text: {
    borderColor: 'black',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 100,
    height: 40,
    borderRadius: 15,
  },
  padding_bt: {
    paddingBottom: 20,
  },
  button_Opacity: {
    backgroundColor: 'red',
  },
});
